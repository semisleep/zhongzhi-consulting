'use strict';

// Allow error to be serialized.
Object.defineProperty(Error.prototype, 'toJSON', {
  value: function () {
    let alt = {};

    for (let key of Object.getOwnPropertyNames(this)) {
      alt[key] = this[key];
    }

    return alt;
  },
  configurable: true,
  enumerable: false,
  writable: true
});

module.exports.config = require('./config');
Object.assign(module.exports, require('./io'), require('./logger'), require('./algorithm'), require('./web'));
