'use strict';

const crypto = require ('crypto');

function hash (content, algorithm) {
  let c = crypto.createHash(algorithm || 'sha256');
  c.update(content, 'utf8');
  return c.digest('hex');
}

function hashWithSalt (content, salt, algorithm) {
  let c = crypto.createHmac(algorithm || 'sha256', salt);
  c.update(content, 'utf8');
  return c.digest('hex');
}

function generateSalt () {
  return crypto.randomBytes(128).toString('base64');
}

module.exports = {hash, hashWithSalt, generateSalt};
