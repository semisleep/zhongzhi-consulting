'use strict';

const fs = require ('fs');
const path = require ('path');

const argv = require ('minimist')(process.argv.slice(2));
let configName = 'config.js';
let configDir = path.normalize(path.join(__dirname, '..'));

let configPath, filePath;
if (argv.c && typeof argv.c === 'string') {
  filePath = argv.c;
  filePath = path.resolve(process.cwd(), filePath);
  if (fs.existsSync(filePath)) {
    configPath = filePath;
  }
} else {
  while (configDir) {
    //require('console').log(configDir);
    filePath = path.join(configDir, configName);
    if (fs.existsSync(filePath)) {
      configPath = filePath;
      break;
    }
    if (configDir === '/') {
      configDir = null;
    } else {
      configDir = path.dirname(configDir);
    }
  }
}

let config;

if (configPath) {
  config = require(configPath);
  config.$name = path.basename(configPath);
  config.$dir = path.dirname(configPath);
  config.$path = configPath;
}

module.exports = config;
