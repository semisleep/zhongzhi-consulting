'use strict';

const _ = require ('lodash');
const fs = require ('fs');
const path = require ('path');
const config = require ('./config');
const logger = require ('./logger');
const io = require ('./io');

exports.jsonError = (res, e) => {
  logger.error('Error handling JSON request:', e.message ? e.message : e);
  if (e.stack) {
    logger.error(e.stack);
  }
  if (e.errors) {
    if (e.errors.length) {
      for (let error of e.errors) {
        logger.error('Error handling JSON request:', error.message);
      }
    } else {
      logger.error('Error handling JSON request:', e.errors);
    }
  }
  if (_.isString(e)) {
    // if error is simply a string, treat it as invalid argument error which would be displayed to client directly
    res.status(400).json({message: e});
  } else {
    res.status(e.status || 500).json({
      key: e.key,
      message: e.message ? e.message : e,
      error: config.production ? null : e.stack
    });
  }
};

exports.InvalidArgumentError = function (message, key) {
  this.message = message || '';
  this.key = key;
  this.status = 400;
};

exports.AuthorizationError = function(message) {
  this.message = message || '';
  this.status = 401;
};

exports.PermissionError = function(message) {
  this.message = message || '';
  this.status = 403;
};

exports.ResourceNotFoundError = function(message) {
  this.message = message || '';
  this.status = 404;
};

exports.logExpressError = (e, req, res, next) => {
  logger.error('Error handling request:', e.message ? e.message : e);
  if (e.stack) {
    logger.error(e.stack);
  }
  if (e.errors) {
    for (let error of e.errors) {
      logger.error('Error handling request:', error.message);
    }
  }
  next(e);
};

exports.handleMultiPartForm = (req, filesLimit) => {
  // include 3rd party libs only when they are needed
  const uuid = require ('node-uuid');
  const Busboy = require ('busboy');

  filesLimit = filesLimit || 1;
  let result = {
    files: {},
    fields: {},
    filesLimitExceeded: false
  };

  let busboy = new Busboy({
    headers: req.headers,
    limits: {
      files: filesLimit
    }
  });
  busboy.on('file', (fieldName, file, fileName, encoding, mimeType) => {
    logger.debug('File [%s] of field [%s], encoding [%s], mime type [%s]', fileName, fieldName, encoding, mimeType);
    fileName = decodeURIComponent(fileName);
    file.on('data', data => {
      logger.debug('File [%s] got %d bytes', fileName, data.length);
    });
    io.createStreamPromise(file).then(() => {
      logger.debug('File [%s] uploaded', fileName);
    });
    let resultFile = {};
    result.files[fieldName] = resultFile;
    resultFile.name = fileName;
    let generatedFileName = uuid.v4() + path.extname(fileName);
    let fullPath = path.join(io.getTempUploadDir(), generatedFileName);
    resultFile.path = fullPath;
    let stream = fs.createWriteStream(fullPath);
    resultFile.upload = io.createStreamPromise(file.pipe(stream)).then(() => {
      logger.debug('File [%s] saved', fileName);
    });
  });
  busboy.on('field', (fieldName, val, fieldNameTruncated, valTruncated) => {
    logger.debug('Field [%s], value: %s, field truncated: %s, value truncated: %s', fieldName, JSON.stringify(val), fieldNameTruncated, valTruncated);
    if (result.fields[fieldName] === undefined) {
      result.fields[fieldName] = val;
    } else if (!isArray(result.fields[fieldName])) {
      result.fields[fieldName] = [result.fields[fieldName]];
      result.fields[fieldName].push(val);
    } else {
      result.fields[fieldName].push(val);
    }
  });
  busboy.on('filesLimit', () => {
    result.filesLimitExceeded = true;
  });

  return io.createStreamPromise(req.pipe(busboy))
    .then(() => {
      logger.debug('Done parsing form!');
      if (!Object.keys(result.files).length) {
        throw new exports.InvalidArgumentError('Should upload a file.');
      }
      if (result.filesLimitExceeded) {
        for (let key of Object.keys(result.files)) {
          io.deleteFile(result.files[key].path);
        }
        throw new exports.InvalidArgumentError('Should not upload more than ' + filesLimit + ' file.');
      }
      let promises = [];
      for (let key of Object.keys(result.files)) {
        promises.push(result.files[key].upload);
      }
      return Promise.all(promises);
    })
    .then(() => {
      return result;
    });
};

function isArray(subject) {
  return subject && !!subject.length;
}
