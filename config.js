let config = {};

config.production = false;

config.web = {
  port: 3344,
  resourceAge: 1000 * 60 * 60 // an hour
};

config.distAge = 3600000; // an hour

config.sessionSecret = 'top_secret';

module.exports = config;