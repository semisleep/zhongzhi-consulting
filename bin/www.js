#!/usr/bin/env node
'use strict';

const utils = require('../utils');
const config = utils.config;
const app = require('../app');

app.set('port', config.web.port || 80);

let server = app.listen(app.get('port'), () => {
  console.log('Express server listening on port ', server.address().port);
});
