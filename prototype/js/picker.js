$("#picker").picker({
  toolbarTemplate: '<header class="bar bar-nav">\
  <button class="button button-link pull-right close-picker">确定</button>\
  <h1 class="title">请选择类别</h1>\
  </header>',
  cols: [
    {
      textAlign: 'center',
      values: ['家庭教育', '心理教育', '学校教育', '健康与安全', '家长教育 ','特殊儿童']
    }
  ]
});
$("#sex").picker({
  toolbarTemplate: '<header class="bar bar-nav">\
  <button class="button button-link pull-right close-picker">确定</button>\
  <h1 class="title">请选择性别</h1>\
  </header>',
  cols: [
    {
      textAlign: 'center',
      values: ['男', '女']
    }
  ]
});


    $("#datetime-picker").datetimePicker({
      toolbarTemplate: '<header class="bar bar-nav">\
      <button class="button button-link pull-right close-picker">确定</button>\
      <h1 class="title">选择日期</h1>\
      </header>'
    });





