window.config = window.config || {};

var config = window.config;

config.codes = {
  sort: [
    {code: 'all', text: '综合排序'},
    {code: 'smart', text: '智能排序'},
    {code: 'hotest', text: '热门程度'},
    {code: 'latest', text: '最新活动'},
    {code: 'nearest', text: '离我最近'}
  ],
  activityType: [
    {code: 'all', text: '全部'},
    {code: 'outdoor', text: '户外活动'},
    {code: 'family', text: '亲子活动'},
    {code: 'kid', text: '儿童活动'},
    {code: 'speech', text: '专题讲座'},
    {code: 'community', text: '公益活动'}
  ],
  activitySpot: ['全部', '黄浦区', '越秀区', '海珠区', '白云区', '荔湾区', '番禺区', '花都区', '其他'],
  activityDate: [
    {code: 'all', text: '全部'},
    {code: 'weekend', text: '周末'},
    {code: 'workday', text: '工作日'}
  ],
  activityState: [
    {code: 'all', text: '全部'},
    {code: 'pending', text: '未开始', className: 'img-state-notbegin'},
    {code: 'ongoing', text: '报名中', className: 'img-state-ongoing'},
    {code: 'closed', text: '已结束', className: 'img-state-close'}
  ]
};
