'use strict';

import Vue from 'vue';
import VueRouter from 'vue-router';
import * as VueResource from 'vue-resource';

import SPA from './vue-components/SPA.vue';
import Login from './vue-components/Login.vue';
import Home from './vue-components/Home.vue';
import ActivityList from './vue-components/ActivityList.vue';
import ContentList from './vue-components/ContentList.vue';
import Me from './vue-components/Me.vue';

Vue.use(VueRouter);
Vue.use(VueResource);

const router = new VueRouter({
  mode: 'history',
  base: '/spa/',
  routes: [
    {path: '/login', component: Login},
    {path: '/', component: Home},
    {path: '/activity-list', component: ActivityList},
    {path: '/content-list', component: ContentList},
    {path: '/me', component: Me}
  ],
});

new Vue({
  router,
  el: '#app',
  render (h) {
    return h(SPA);
  }
});
