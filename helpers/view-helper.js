'use strict';

const path = require('path');
const fs = require('fs');
const _ = require('lodash');

const utils = require('../utils');
const config = utils.config;

function setUp(app) {
  app.locals.scriptTag = scriptTag;
  app.locals.cssTag = cssTag;
}

let resourceUrlCache = {};

function getUrl(rawUrl) {
  let url;
  if (rawUrl.indexOf('http://') < 0 && rawUrl.indexOf('https://') < 0 && rawUrl.charAt(0) !== '/') { // relative url
    if (resourceUrlCache[rawUrl]) {
      url = resourceUrlCache[rawUrl];
    } else if (config.production) {
      utils.debug('Initialize script url cache for %s', rawUrl);
      let urlLocation = path.join(__dirname, '..', '/client/' + rawUrl);
      let content = fs.readFileSync(urlLocation, {encoding: 'utf-8'});
      url = '/' + rawUrl;
      let hash = utils.hash(content);
      url = url + '?h=' + hash;
      resourceUrlCache[rawUrl] = url;
    } else {
      url = '/' + rawUrl;
    }
  } else {
    url = rawUrl;
  }
  return url;
}

function scriptTag(rawUrl) {
  return '<script type="text/javascript" src="' + getUrl(rawUrl) + '"></script>';
}

function cssTag(rawUrl) {
  return '<link href="' + getUrl(rawUrl) + '" rel="stylesheet" />';
}

module.exports = {
  setUp: setUp
};