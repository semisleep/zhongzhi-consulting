'use strict';

// require libs for server codes
const moment = require('moment');

function empty(s) {
  return s || '-';
}

function formatYear(date) {
  return date ? moment(date).format('YYYY') : '';
}

function formatYearMonth(date) {
  return date ? moment(date).format('YYYY-MM') : '';
}

function formatDate(date) {
  return date ? moment(date).format('YYYY-MM-DD') : '';
}

function formatDateLong(date) {
  return date ? moment(date).format('MMM DD, YYYY') : '';
}

function formatDateVerbose(date, superscript) {
  if (!date) {
    return '';
  } else if (!superscript) {
    return moment(date).format('dddd, MMMM Do YYYY');
  } else {
    date = moment(date);
    var day = date.format('Do');
    day = day.substring(0, day.length - 2) + '<sup>' + day.substring(day.length - 2) + '</sup>';
    return date.format('dddd, MMMM ') + day + date.format(' YYYY');
  }
}

function formatFullDate(date) {
  return date ? moment(date).format('YYYY-MM-DD HH:mm') : '';
}

function formatTime(date) {
  return date ? moment(date).format('HH:mm') : '';
}

function formatLocalizedDate(date) {
  if (!date) {
    return '';
  }
  date = moment(date);
  let now = moment();
  let diff = now.diff(date, 'minute');
  if (diff >= 0) {
    if (diff < 2) {
      return formatter.i18n.t('date.just_now');
    } else if (diff < 60) {
      return formatter.i18n.t('date.minutes_ago', [diff]);
    } else if (now.isSame(date, 'day')) {
      return formatter.i18n.t('date.today', [date.format('HH:mm')]);
    } else if (now.add(-1, 'day').isSame(date, 'date')) {
      return formatter.i18n.t('date.yesterday', [date.format('HH:mm')]);
    }
  }
  return date.format(formatter.i18n.t('date.format'));
}

function formatLocalizedTime(date) {
  if (!date) {
    return '';
  }
  return moment(date).format(formatter.i18n.t('date.time_format'));
}

function formatLocalizedDay(date) {
  if (!date) {
    return '';
  }
  let diff = moment().diff(date, 'days');
  if (diff === 0) {
    return formatter.i18n.t('date.day_today');
  } else if (diff === 1) {
    return formatter.i18n.t('date.day_yesterday');
  } else {
    return moment(date).format(formatter.i18n.t('date.day_format'));
  }
}

function formatSeconds(seconds) {
  function formatPart(n) {
    return (n > 9 ? '' : '0') + n;
  }
  let minutes = Math.floor(seconds / 60);
  seconds = seconds % 60;
  return formatPart(minutes) + ':' + formatPart(seconds);
}

function formatInt(i) {
  if (i === undefined || i === null || isNaN(i)) {
    return '';
  }
  let value = i < 0 ? '-' : '';
  value += Math.abs(i).toFixed(0);
  return value;
}

function formatFloat(f, digits) {
  if (f === undefined || f === null || isNaN(f)) {
    return '';
  }
  digits = digits || 2;
  let value = f < 0 ? '-' : '';
  value += Math.abs(f).toFixed(digits);
  return value;
}

function formatIntWithCommas(i) {
  return numberWithCommas(formatInt(i));
}

function formatFloatWithCommas(f, digits) {
  return numberWithCommas(formatFloat(f, digits));
}

function numberWithCommas(x) {
  let index = x.indexOf('.');
  if (index >= 0) {
    return x.substring(0, index).replace(/\B(?=(\d{3})+(?!\d))/g, ',') + x.substring(index);
  } else {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  }
}

function formatSize(size) {
  if (!size) {
    return '0 KB';
  } else if (size > 1024 * 1024) {
    return formatFloat(size / 1024 / 1024) + ' GB';
  } else if (size > 1024) {
    return formatFloat(size / 1024) + ' MB';
  } else {
    return size + ' KB';
  }
}

function formatDuration(seconds) {
  let minutes = Math.floor(seconds / 60);
  seconds = seconds % 60;
  return `${numWithPadding(minutes)}:${numWithPadding(seconds)}`;

  function numWithPadding(num) {
    num = num.toString();
    if (num.length <= 1) {
      num = `0${num}`;
    }
    return num;
  }
}

function getUserName(user) {
  if (!user) {
    return '';
  }
  let name = user.firstName || '';
  if (user.lastName) {
    name += ' ' + user.lastName;
  }
  return name.trim();
}

function getDate(date) {
  let dateString = moment(date).format('YYYY-MM-DD');
  return moment(dateString + ' 00:00 +0000', 'YYYY-MM-DD HH:mm ZZ').toDate();
}

function getLocalDate(date) {
  let dateString = moment(date).format('YYYY-MM-DD');
  return moment(dateString + ' 00:00', 'YYYY-MM-DD HH:mm').toDate();
}

let formatter = {};

formatter.i18n = {
  t () {
    return '';
  }
};
formatter.empty = empty;
formatter.formatYear = formatYear;
formatter.formatYearMonth = formatYearMonth;
formatter.formatDate = formatDate;
formatter.formatDateLong = formatDateLong;
formatter.formatDateVerbose = formatDateVerbose;
formatter.formatFullDate = formatFullDate;
formatter.formatTime = formatTime;
formatter.formatLocalizedDate = formatLocalizedDate;
formatter.formatLocalizedTime = formatLocalizedTime;
formatter.formatLocalizedDay = formatLocalizedDay;
formatter.formatSeconds = formatSeconds;
formatter.formatInt = formatInt;
formatter.formatFloat = formatFloat;
formatter.formatIntWithCommas = formatIntWithCommas;
formatter.formatFloatWithCommas = formatFloatWithCommas;

[1, 2, 3, 4, 5, 6, 7, 8, 9].forEach(i => {
  formatter['formatFloat' + i] = function (f) {
    return formatFloat(f, i);
  };
  formatter['formatFloat' + i + 'WithCommas'] = function (f) {
    return formatFloat(f, i);
  };
});

formatter.formatSize = formatSize;
formatter.formatDuration = formatDuration;
formatter.getUserName = getUserName;
formatter.getDate = getDate;
formatter.getLocalDate = getLocalDate;

module.exports = formatter;
