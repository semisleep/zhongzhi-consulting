'use strict';

const _ = require('lodash');
const Promise = require('bluebird');
let request = Promise.promisify(require('request'), {
  multiArgs: true
});
const utils = require('../utils');
const config = utils.config;

let API = function (options) {
  this.options = options;
};

function urlJoin(a, b) {
  return _.trimEnd(a, '/') + '/' + _.trimStart(b, '/');
}

API.prototype.send = function (method, path, data) {
  let url = urlJoin(this.options.endpoint, path);
  let options = {
    method: method,
    url: url,
    headers: {}
  };

  if (data) {
    if (method.toUpperCase() === 'GET') {
      options.qs = data;
    } else {
      options.headers.contentType = 'application/json';
      options.json = data;
    }
  }

  if (this.params.token) {
    options.headers.Authorization = 'Bearer ' + this.params.token;
  }

  return request(options).spread((response, body) => {
    if (response.statusCode !== 200) {
      let err = new Error('Failed to called the API');
      err.status = response.statusCode;
      err.body = body;
      throw err;
    }
    return JSON.parse(body);
  });
};

module.exports = function (params) {
  let result = new API({
    endpoint: config.apiEndpoint
  });
  result.params = params || {};
  return result;
};
