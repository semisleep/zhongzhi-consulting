'use strict';

const _ = require('lodash');

function getDomain(hostname) {
  let lastDotIndex = hostname.lastIndexOf('.');
  if (lastDotIndex < 0 || !_.isNaN(parseInt(hostname.substring(lastDotIndex + 1)))) {
    return;
  }
  return hostname.substring(hostname.lastIndexOf('.', lastDotIndex - 1) + 1);
}

module.exports.getDomain = getDomain;