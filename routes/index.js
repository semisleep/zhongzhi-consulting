'use strict';

const _ = require('lodash');
const express = require('express');
const MobileDetect = require('mobile-detect');

let router = express.Router();

router.use((req, res, next) => {
  let md = new MobileDetect(req.headers['user-agent']);
  res.locals.isMobile = md.mobile() && !md.tablet();
  res.locals.isTablet = !!md.tablet();
  res.locals.session = req.session;
  _.extend(res.locals, require('../helpers/data-formatter.js'));
  next();
});

router.use('/api-proxy', require('./api-proxy'));

router.use('/api', require('./api'));

router.use((req, res, next) => {
  // redirect ie < 10
  let ua = (req.headers['user-agent'] || '').toLowerCase();
  let ie = (ua.indexOf('msie') != -1) ? parseInt(ua.split('msie')[1]) : false;
  if (ie && ie < 10) {
    res.redirect('/static/browser-not-supported.html');
    return;
  }
  next();
});

router.get(['/spa', '/spa/*'], (req, res) => {
  res.render('spa');
});

module.exports = router;
