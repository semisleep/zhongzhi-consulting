'use strict';

const express = require('express');
let router = express.Router();
const utils = require('../utils');
const config = utils.config;
const proxy = require('http-proxy').createProxyServer();
const url = require('url');

router.all('/*', (req, res, next) => {
    req.headers.host = url.parse(config.apiEndpoint).hostname;
    proxy.web(req, res, {target: config.apiEndpoint}, e => {
        // error occurs
        let err = new Error('failed to connect to the proxy server');
        e.status = 500;
        next(err);
    });
});

module.exports = router;
