'use strict';

const express = require('express');
const config = require('../client/app/scripts/config');

let router = express.Router();

router.post('/login', (req, res) => {
  let {phone, password} = req.body;
  if (!phone || !password) {
    res.json({success: false});
  } else {
    res.json({success: true});
  }
});

router.get('/activities', (req, res) => {
  let query = req.query;
  let list = [];
  for (let i = 0; i < 20; i++) {
    let state = select(config.codes.activityState);
    list.push({
      title: '最国际+最美式+最新潮夏令营，游美上海营酷炫开营！',
      type: select(config.codes.activityType.slice(1).map(function (type) {
        return type.code;
      })),
      date: select(['2017-09-10', '2017-09-11', '2017-09-12', '2017-09-13', '2017-09-14']),
      dateType: select(config.codes.activityDate.slice(1).map(function (date) {
        return date.code;
      })),
      price: '免费',
      address: select(config.codes.activitySpot.slice(1)),
      count: select([3, 4, 5, 6, 7, 8, 9]),
      stateClass: state.className,
      stateCn: state.text,
      state: state.code,
      show: true
    });
  }
  list = list.filter(function (item) {
    let result = true;
    let filter = query.filter;
    if (filter.activityType && filter.activityType !== 'all') {
      result = result && item.type === filter.activityType;
    }
    if (filter.activitySpot && filter.activitySpot !== '全部') {
      result = result && item.address === filter.activitySpot;
    }
    if (filter.activityDate && filter.activityDate !== 'all') {
      result = result && item.dateType === filter.activityDate;
    }
    if (filter.activityState && filter.activityState !== 'all') {
      result = result && item.state === filter.activityState;
    }
    return result;
  });
  if (query.sort && query.sort !== 'all') {
    list = list.sort(function (item1, item2) {
      if (query.sort === 'hotest') {
        return item2.count - item1.count;
      } else if (query.sort === 'latest') {
        return item2.date - item1.date;
      } else if (query.sort === 'nearest') {
        return item1.address - item2.address;
      }
    });
  }
  res.json(list);
});


function select(items) {
  return items[Math.floor(Math.random() * items.length)];
}

module.exports = router;
